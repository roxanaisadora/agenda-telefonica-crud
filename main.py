from flask import Flask,request
from flask_restx import Api, Resource

from controller.controllerUsuario import ControllerUsuario

app = Flask(__name__)
api = Api(app)


api.add_resource(ControllerUsuario, "/usuario")

if __name__ == '__main__':
  app.run(port=8089, debug=True)