import pymongo

class ConexionMongo:

    def __init__(self):
        myclient = pymongo.MongoClient("mongodb://localhost27017/")
        mydb = myclient["agendatelefonica"]
        self.cursor = mydb["py"]


    def agregar(self, objList):
        x=self.cursor.insert_many(objList)
        print(x.insert_ids)

    def traer(self,myquery):
        mydoc = self.cursor.find(myquery)
        for x in mydoc:
            print(x)

    def actualizar(self,myquery,newvalues):
        x= self.cursor.update_one(myquery,newvalues)
        print(x.modified_count, "documento actualizado")

    def borrar(self,myquery):
        x= self.cursor.delete_many(myquery)
        print(x.delete_count, "documento borrado")