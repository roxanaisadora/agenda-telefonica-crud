from flask_restx import Api, Resource
from mongodb.dbMongo import ConexionMongo
from flask import request

conexion = ConexionMongo()

#Armando la estructura del crud
class ControllerUsuario(Resource):
  #Definiendo el crud para los usuarios

  def post(self):
    nuevocontacto = request.get_json()
    self.conexion.agregar(nuevocontacto)
    return f'se ha creado el ID', 201

  def get(self):
    mostrarcontacto = request.get_json()
    self.conexion.traer(mostrarcontacto)
    return f'se tiene los contactos', 200

  def delete(self, ID):
    eliminarcontacto = request.get_json()
    self.conexion.borrar(eliminarcontacto)
    return f'Se ha eliminado el ID {ID}'

  def put(self, ID, newvalues):
    actualizarcontacto = request.get_json()
    self.conexion.actualizar(actualizarcontacto, newvalues)
    return f'Se ha actualizado el {ID}'


